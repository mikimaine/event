<?php

namespace App\Http\Requests\Backend\Clubs;

use App\Http\Requests\Request;

/**
 * Class UpdateUserRequest.
 */
class UpdateClubsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  access()->allow('update-clubs');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [ ];
    }
}
