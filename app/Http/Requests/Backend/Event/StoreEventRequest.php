<?php

namespace App\Http\Requests\Backend\Event;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreEventRequest.
 */
class StoreEventRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  access()->allow('store-event');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|max:255',
            'event_date' => 'required|max:255'
        ];
    }
}
