<?php

namespace App\Http\Requests\Backend\Hall;

use App\Http\Requests\Request;

/**
 * Class UpdateUserRequest.
 */
class UpdateHallRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  access()->allow('update-hall');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required',
        ];
    }
}
