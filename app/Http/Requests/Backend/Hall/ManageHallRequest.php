<?php

namespace App\Http\Requests\Backend\Hall;

use App\Http\Requests\Request;

/**
 * Class ManageUserRequest.
 */
class ManageHallRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  access()->allow('manage-hall');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
