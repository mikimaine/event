<?php

namespace App\Http\Requests\Backend\Budget;

use App\Http\Requests\Request;

/**
 * Class UpdateUserRequest.
 */
class UpdateBudgetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  access()->allow('update-budget');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required',
            'budget_date'  => 'required',
            'amount'  => 'required',
            'department_id'  => 'required',
            'approved_by'  => 'required',
        ];
    }
}
