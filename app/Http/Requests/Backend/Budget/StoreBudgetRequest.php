<?php

namespace App\Http\Requests\Backend\Budget;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreUserRequest.
 */
class StoreBudgetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  access()->allow('store-budget');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required',
            'budget_date'  => 'required',
            'amount'  => 'required',
            'department_id'  => ['required', Rule::unique('buget')],
            'approved_by'  => 'required',
        ];
    }
}
