<?php

namespace App\Http\Requests\Backend\Budget;

use App\Http\Requests\Request;

/**
 * Class ManageUserRequest.
 */
class ManageBudgetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  access()->allow('manage-budget');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
