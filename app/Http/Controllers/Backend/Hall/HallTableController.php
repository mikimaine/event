<?php

namespace App\Http\Controllers\Backend\Hall;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Department\ManageDepartmentRequest;
use App\Http\Requests\Backend\Hall\ManageClubsRequest;
use App\Http\Requests\Backend\Hall\ManageHallRequest;
use App\Repositories\Backend\Hall\HallRepository;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Access\User\UserRepository;

/**
 * Class UserTableController.
 */
class HallTableController extends Controller
{

    /**
     * @var HallRepository
     */
    protected $hallRepository;


    /**
     * HallTableController constructor.
     * @param HallRepository $hallRepository
     */
    public function __construct(HallRepository $hallRepository)
    {
        $this->hallRepository = $hallRepository;
    }


    /**
     * @param ManageHallRequest $request
     * @return mixed
     */
    public function __invoke(ManageHallRequest $request)
    {
        return Datatables::of($this->hallRepository->getForDataTable($request->get('trashed')))
            ->escapeColumns(['name', 'max_position'])
            ->addColumn('actions', function ($hall) {
                return $hall->action_buttons;
            })
            ->withTrashed()
            ->make(true);
    }
}
