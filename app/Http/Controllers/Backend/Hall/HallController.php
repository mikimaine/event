<?php
/**
 * ETM SOFTWARE PLC
 * Created For UNHCR with LARAVEL FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 3/3/17
 * Time: 12:56 AM
 */

namespace App\Http\Controllers\Backend\Hall;

use App\Http\Requests\Backend\Hall\UpdateHallRequest;
use App\Models\Hall\Hall;
use App\Models\Budget\Budget;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Hall\HallRepository;
use App\Http\Requests\Backend\Hall\StoreHallRequest;
use App\Http\Requests\Backend\Hall\ManageHallRequest;
use App\Http\Requests\Backend\Budget\UpdateBudgetRequest;

/**
 * Class HallController
 * @package App\Http\Controllers\Backend\Hall
 */
class HallController extends Controller
{
    /**
     * @var HallRepository
     */
    private $hallRepository;

    /**
     * HallController constructor.
     * @param HallRepository $hallRepository
     */
    public function __construct(HallRepository $hallRepository)
    {

        $this->hallRepository = $hallRepository;
    }


    /**
     * @param ManageHallRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageHallRequest $request)
    {

        return view('backend.hall.index');
    }


    /**
     * @param ManageHallRequest $request
     */
    public function create(ManageHallRequest $request)
    {

    }


    /**
     * @param StoreHallRequest $request
     * @return mixed
     */
    public function store(StoreHallRequest $request)
    {
        $this->hallRepository->create($request->all());
        return redirect()->route('admin.hall.hall.index')->withFlashSuccess('New Hall Created successfully!');

    }


    /**
     * @param Budget $user
     * @param ManageHallRequest $request
     */
    public function show(Budget $user, ManageHallRequest $request)
    {

    }


    /**
     * @param Hall $hall
     * @param ManageHallRequest $request
     * @return mixed
     */
    public function edit(Hall $hall, ManageHallRequest $request)
    {
        return view('backend.hall.edit')->withHall($hall);
    }


    /**
     * @param Hall $hall
     * @param UpdateHallRequest $request
     * @return mixed
     */
    public function update(Hall $hall, UpdateHallRequest $request)
    {
        $this->hallRepository->update($hall,$request->all());
        return redirect()->route('admin.hall.hall.index')
            ->withFlashSuccess('Hall Updated successfully!');
    }


    /**
     * @param Hall $hall
     * @param ManageHallRequest $request
     * @return mixed
     */
    public function destroy(Hall $hall, ManageHallRequest $request)
    {
        $this->hallRepository->destroy($hall);
        return redirect()->route('admin.hall.hall.index')->withFlashSuccess('Hall Deleted');
    }


}