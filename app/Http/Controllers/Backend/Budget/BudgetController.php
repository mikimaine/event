<?php

namespace App\Http\Controllers\Backend\Budget;

use Carbon\Carbon;
use App\Models\Budget\Budget;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Budget\BudgetRepository;
use App\Http\Requests\Backend\Budget\StoreBudgetRequest;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Http\Requests\Backend\Budget\UpdateBudgetRequest;
use App\Http\Requests\Backend\Budget\ManageBudgetRequest;
use App\Repositories\Backend\Department\DepartmentRepository;

/**
 * Class BudgetController
 * @package App\Http\Controllers\Backend\Budget
 */
class BudgetController extends Controller
{
    /**
     * @var BudgetRepository
     */
    private $budgetRepository;

    /**
     * BudgetController constructor.
     * @param BudgetRepository $budgetRepository
     */
    public function __construct(BudgetRepository $budgetRepository)
    {

        $this->budgetRepository = $budgetRepository;
    }


    /**
     * @param ManageBudgetRequest $request
     * @param DepartmentRepository $departmentRepository
     * @return mixed
     */
    public function index(ManageBudgetRequest $request, DepartmentRepository $departmentRepository,UserRepository $userRepository)
    {
        return view('backend.budget.index')
                ->withDepartments($departmentRepository->getAll()->mapWithKeys(function ($item) {
                    return [$item['id'] => $item['name']];
                }))
                ->withUsers($userRepository->getAll()->mapWithKeys(function ($item) {
                    return [$item['id'] => $item['name']];
                }));
    }


    /**
     * @param ManageBudgetRequest $request
     */
    public function create(ManageBudgetRequest $request)
    {

    }


    /**
     * @param StoreBudgetRequest $request
     * @return mixed
     */
    public function store(StoreBudgetRequest $request)
    {
         $dates = explode(' - ',$request->budget_date);
         $request['start_date'] = Carbon::createFromFormat('m/d/Y',$dates[0])->toDateString();
         $request['end_date'] = Carbon::createFromFormat('m/d/Y',$dates[1])->toDateString();
        $this->budgetRepository->create($request->all());
        return redirect()->route('admin.budget.budget.index')->withFlashSuccess('New Budget Created successfully!');

    }


    /**
     * @param Budget $user
     * @param ManageBudgetRequest $request
     */
    public function show(Budget $user, ManageBudgetRequest $request)
    {

    }


    /**
     * @param Budget $budget
     * @param ManageBudgetRequest $request
     * @param DepartmentRepository $departmentRepository
     * @param UserRepository $userRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Budget $budget, ManageBudgetRequest $request,DepartmentRepository $departmentRepository,UserRepository $userRepository)
    {
        return view('backend.budget.edit')
            ->withBudget($budget)
            ->withDepartments($departmentRepository->getAll()->mapWithKeys(function ($item) {
                return [$item['id'] => $item['name']];
            }))
            ->withUsers($userRepository->getAll()->mapWithKeys(function ($item) {
                return [$item['id'] => $item['name']];
            }));;
    }


    /**
     * @param Budget $budget
     * @param UpdateBudgetRequest $request
     */
    public function update(Budget $budget, UpdateBudgetRequest $request)
    {
        $dates = explode(' - ',$request->budget_date);
        $request['start_date'] = Carbon::createFromFormat('m/d/Y',$dates[0])->toDateString();
        $request['end_date'] = Carbon::createFromFormat('m/d/Y',$dates[1])->toDateString();
        $this->budgetRepository->update($budget,$request->all());
        return redirect()->route('admin.budget.budget.index')->withFlashSuccess('Budget Updated successfully!');

    }


    /**
     * @param Budget $budget
     * @param ManageBudgetRequest $request
     */
    public function destroy(Budget $budget, ManageBudgetRequest $request)
    {
        $this->budgetRepository->destroy($budget);
        return redirect()->route('admin.budget.budget.index')->withFlashSuccess('Budget Deleted');
    }


}