<?php

namespace App\Http\Controllers\Backend\Budget;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Budget\ManageBudgetRequest;
use App\Http\Requests\Backend\Department\ManageDepartmentRequest;
use App\Repositories\Backend\Budget\BudgetRepository;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Access\User\UserRepository;

/**
 * Class UserTableController.
 */
class BudgetTableController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $budgets;

    /**
     * @param BudgetRepository $budgets
     */
    public function __construct(BudgetRepository $budgets)
    {
        $this->budgets = $budgets;
    }

    /**
     * @param ManageBudgetRequest $request
     * @return mixed
     */
    public function __invoke(ManageBudgetRequest $request)
    {
        return Datatables::of($this->budgets->getForDataTable($request->get('trashed')))
            ->escapeColumns(['name', 'department_id', 'start_date', 'end_date', 'amount'])
            ->editColumn('budget_date', function ($budget) {
                return $budget->start_date .' - '.$budget->end_date;
            })->editColumn('approved_by', function ($budget) {
                 return $budget->user->name;
            })->editColumn('department_id', function ($budget) {
                 return $budget->department->name;
            })
            ->addColumn('actions', function ($hall) {
                return $hall->action_buttons;
            })
            ->withTrashed()
            ->make(true);
    }
}
