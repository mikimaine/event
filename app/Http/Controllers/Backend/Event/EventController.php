<?php

namespace App\Http\Controllers\Backend\Event;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Event\ManageEventRequest;
use App\Http\Requests\Backend\Event\StoreEventRequest;
use App\Http\Requests\Backend\Event\UpdateEventRequest;
use App\Models\Budget\Budget;
use App\Models\CoHost\CoHost;
use App\Models\Event\Event;
use App\Notifications\EventCohost;
use App\Notifications\EventCohostAccept;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Repositories\Backend\Department\DepartmentRepository;
use App\Repositories\Backend\Event\EventRepository;
use App\Repositories\Backend\Hall\HallRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

/**
 * Class HallController
 * @package App\Http\Controllers\Backend\Hall
 */
class EventController extends Controller
{
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * EventController constructor.
     * @param EventRepository $eventRepository
     * @param UserRepository $userRepository
     */
    public function __construct(EventRepository $eventRepository,UserRepository $userRepository)
    {

        $this->eventRepository = $eventRepository;
        $this->userRepository = $userRepository;
    }


    /**
     * @param ManageEventRequest $request
     * @param DepartmentRepository $departmentRepository
     * @param UserRepository $userRepository
     * @param HallRepository $hallRepository
     * @return mixed
     */
    public function index(ManageEventRequest $request, DepartmentRepository $departmentRepository,
                          UserRepository $userRepository, HallRepository $hallRepository)
    {

        return view('backend.event.index')
            ->withDepartments($departmentRepository->getAll()->mapWithKeys(function ($item) {
                return [$item['id'] => $item['name']];
            }))
            ->withUsers($userRepository->getAll()->mapWithKeys(function ($item) {
                return [$item['id'] => $item['name']];
            }))
            ->withHalls($hallRepository->getAll()->mapWithKeys(function ($item) {
                return [$item['id']=> $item['name'].' (max '.$item['max_position'].' position)'];
            }))->withCohost($userRepository->getByRole(['College','Administrator','Offices','Student Union'])
               ->mapWithKeys(function ($item) {
                return [$item['id'] => $item['name']];
            }));
    }

    /**
     * @param ManageEventRequest $request
     */
    public function create(ManageEventRequest $request)
    {

    }


    /**
     * Used for creating new Event
     * @param StoreEventRequest $request
     * @return mixed
     */
    public function store(StoreEventRequest $request)
    {
        $dates = explode(' - ',$request->event_date);
        $request['requested_start_time'] = Carbon::createFromFormat('m/d/Y',$dates[0])->toDateString();
        $request['requested_end_time']   = Carbon::createFromFormat('m/d/Y',$dates[1])->toDateString();

        /**
         * Check if there is no other event with the same date and time and Hall
         */
        if ($this->eventRepository->checkEventDate($request)->count() == 0){

            if (!Carbon::now()->greaterThan(Carbon::createFromFormat('Y-m-d',$request['requested_start_time']))  ){


            $budget = Budget::where('department_id', '=', $request->department_id)->first();
            /**
             * It will check if the event creator have efficient budget to prepare the event
             */
            if ($budget->amount >= $request->budget){
                // Upload the image ....
                $request['image_path'] = $request->file('image')->store('image_data','public');

                // store the event to the database
                $event =  $this->eventRepository->create($request->all());
                $result ='';
                // adjust the budget for the department
                $budget->amount = $budget->amount - $request->budget;
                $budget->update();

                /**
                 * Check if co-hosting request is there
                 * if there is co-hosting request,
                 * It will send the notification
                 */
                if ($request['co_host'] != ''){
                    $result = $this->sendCoHost($event->id,$request['co_host']);
                }
                return redirect()->route('admin.event.event.index')->withFlashSuccess('New Event Created successfully! '. $result);
            }

            return back()->withFlashDanger('Does not have enough Budget to create event.You Only have '.$budget->amount.' Birr left');

             }

            return back()->withFlashDanger('You cant create the an event for the past dates');
        }
        return back()->withFlashDanger('You cant create the same event at the same time/position');

    }


    /**
     * @param Event $event
     * @param ManageEventRequest $request
     */
    public function show(Event $event, ManageEventRequest $request)
    {

    }


    /**
     * @param Event $event
     * @param ManageEventRequest $request
     * @param DepartmentRepository $departmentRepository
     * @param UserRepository $userRepository
     * @param HallRepository $hallRepository
     * @return mixed
     */
    public function edit(Event $event, ManageEventRequest $request, DepartmentRepository $departmentRepository,
                         UserRepository $userRepository, HallRepository $hallRepository)
    {
        return view('backend.event.edit')
            ->withEvent($event)
            ->withDepartments($departmentRepository->getAll()->mapWithKeys(function ($item) {
                return [$item['id'] => $item['name']];
            }))
            ->withUsers($userRepository->getAll()->mapWithKeys(function ($item) {
                return [$item['id'] => $item['name']];
            }))
            ->withHalls($hallRepository->getAll()->mapWithKeys(function ($item) {
                return [$item['id']=> $item['name'].' (max '.$item['max_position'].' position)'];
            }))->withCohost($userRepository->getByRole(['College','Administrator','Offices','Student Union'])
                ->mapWithKeys(function ($item) {
                    return [$item['id'] => $item['name']];
                }));
    }


    /**
     * @param Event $event
     * @param UpdateEventRequest $request
     * @return mixed
     */
    public function update(Event $event, UpdateEventRequest $request)
    {
        if ($event->created_by == Auth::user()->id || access()->hasRole(1) ){

            $dates = explode(' - ',$request->event_date);
            $request['requested_start_time'] = Carbon::createFromFormat('m/d/Y',$dates[0])->toDateString();
            $request['requested_end_time']   = Carbon::createFromFormat('m/d/Y',$dates[1])->toDateString();

            $request['image_path'] = 'img/';
            $this->eventRepository->update($event,$request->all());
            return redirect()->route('admin.event.event.index')->withFlashSuccess('Event Updated successfully!');
        }
        return back()->withFlashDanger('You can\'t Edit this event because you are not the owner');
    }


    /**
     * Deleting Event
     * @param Event $event
     * @param ManageEventRequest $request
     * @return mixed
     */
    public function destroy($event, ManageEventRequest $request)
    {
        $eventO =    $this->eventRepository->findOrThrowException($event);
        if ($eventO->created_by == Auth::user()->id || access()->hasRole(1) ){
            $this->eventRepository->destroy($event);
            return redirect()->route('admin.event.event.index')->withFlashSuccess('Event Deleted');
        }
            return back()->withFlashDanger('You can\'t Deleted this event');
    }

    /**
     * Sends Co-hosting notification to the appropriate user
     * @param $event_id
     * @param $requested_id
     * @return string
     */
    public function sendCoHost($event_id, $requested_id)
    {
          $requested = $this->userRepository->find($requested_id);
          $event = $this->eventRepository->find($event_id);
          Notification::send($requested,new EventCohost($event));
          return 'notification sent';
    }

    /**
     * When a user accepts co-hosting request
     * @param $event_id
     * @return string
     */
    public function acceptCoHost($event_id,$notification_id)
    {
        $notification = Auth::user()->notifications()->where('id',$notification_id)->first();

        $requested = $this->userRepository->find($notification->data['sender_id']);

        Notification::send($requested,new EventCohostAccept($this->eventRepository->findOrThrowException($event_id)));
        /**
         * This will make the notification as read
         */
        $this->markAsRead($notification_id);

        CoHost::create(['event_id'=>$event_id,'co_host_user_id'=> Auth::user()->id]);

        return back()->withFlashSuccess('You Accepted the invite');
    }

    /**
     * When a use declines a co-hosting request
     * @param $event_id
     * @return mixed
     */
    public function declineCoHost($event_id,$notification_id)
    {
        /**
         * This will make the notification as read
         */
        $this->markAsRead($notification_id);
        return back()->withFlashSuccess('You Declined the invite');
    }

    /**
     * It will get the notification for the current logged in user
     * using the notification id
     * then delete the notification
     * @param $notification_id
     * @return mixed
     */
    private function markAsRead($notification_id)
    {
        $user = Auth::user();
        $notification = $user->notifications()->where('id',$notification_id)->first();
        if ($notification)
        {
           return  $notification->delete();
        }
    }

}