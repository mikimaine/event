<?php

namespace App\Http\Controllers\Backend\Event;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Event\EventRepository;
use App\Http\Requests\Backend\Event\ManageEventRequest;
use App\Repositories\Backend\Access\User\UserRepository;

/**
 * Class UserTableController.
 */
class EventTableController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $eventRepository;

    /**
     * @param EventRepository $eventRepository
     */
    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * @param ManageEventRequest $request
     * @return mixed
     */
    public function __invoke(ManageEventRequest $request)
    {
        return Datatables::of($this->eventRepository->getForDataTable($request->get('trashed')))
            ->escapeColumns(['name', 'department_id', 'type', 'requested_start_time', 'requested_end_time', 'budget', 'hall_id'])
            ->editColumn('date_range', function ($event) {
                return $event->requested_start_time . ' - ' . $event->requested_end_time;
            })
            ->editColumn('department_id', function ($event) {
                return $event->department->name;
            })
            ->editColumn('hall_id', function ($event) {
                return $event->hall->name.' ( max '.$event->hall->max_position.' positions)';
            })
            ->addColumn('actions', function ($event) {
                return $event->action_buttons;
            })
            ->withTrashed()
            ->make(true);
    }
}
