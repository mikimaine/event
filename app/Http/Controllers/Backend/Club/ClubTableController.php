<?php

namespace App\Http\Controllers\Backend\Club;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Club\ClubRepository;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\Clubs\ManageClubsRequest;
use App\Repositories\Backend\Access\User\UserRepository;

/**
 * Class UserTableController.
 */
class ClubTableController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $clubs;

    /**
     * @param ClubRepository $clubs
     */
    public function __construct(ClubRepository $clubs)
    {
        $this->clubs = $clubs;
    }

    /**
     * @param ManageClubsRequest $request
     * @return mixed
     */
    public function __invoke(ManageClubsRequest $request)
    {
        return Datatables::of($this->clubs->getForDataTable($request->get('trashed')))
            ->escapeColumns(['club_name', 'club_head'])
            ->editColumn('club_head', function ($club) {
                return $club->user->name;
            })
            ->addColumn('actions', function ($club) {
                return $club->action_buttons;
            })
            ->withTrashed()
            ->make(true);
    }
}
