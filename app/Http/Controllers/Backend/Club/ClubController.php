<?php

namespace App\Http\Controllers\Backend\Club;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Budget\ManageBudgetRequest;
use App\Http\Requests\Backend\Budget\StoreBudgetRequest;
use App\Http\Requests\Backend\Budget\UpdateBudgetRequest;
use App\Http\Requests\Backend\Clubs\ManageClubsRequest;
use App\Http\Requests\Backend\Clubs\StoreClubsRequest;
use App\Http\Requests\Backend\Clubs\UpdateClubsRequest;
use App\Models\Budget\Budget;
use App\Models\Club\Club;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Repositories\Backend\Club\ClubRepository;

/**
 * Class HallController
 * @package App\Http\Controllers\Backend\Hall
 */
class ClubController extends Controller
{

    public function __construct()
    {

    }


    public function index(ManageBudgetRequest $request, UserRepository $userRepository)
    {

        return view('backend.club.index')
            ->withUsers($userRepository->getByRole(['Lecturer','Student'])->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        }));
    }


    public function create(ManageBudgetRequest $request)
    {

    }


    public function store(StoreClubsRequest $request,ClubRepository $clubRepository)
    {
        $clubRepository->create($request->all());
        return redirect()->route('admin.club.club.index')
            ->withFlashSuccess('New Club Created successfully!');
    }


    public function show(Budget $user, ManageBudgetRequest $request)
    {

    }


    public function edit(Club $club, ManageClubsRequest $request, UserRepository $userRepository)
    {
        return view('backend.club.edit')
                ->withClub($club)
                ->withUsers($userRepository->getByRole(['Lecturer','Student'])->mapWithKeys(function ($item) {
                    return [$item['id'] => $item['name']];
                }));
    }


    public function update(Club $club, UpdateClubsRequest $request,ClubRepository $clubRepository)
    {
        $clubRepository->update($club,$request->all());
        return redirect()->route('admin.club.club.index')
            ->withFlashSuccess('Club Updated successfully!');
    }


    public function destroy($club, ManageBudgetRequest $request,ClubRepository $clubRepository)
    {
        $clubRepository->destroy($club);
        return redirect()->route('admin.club.club.index')->withFlashSuccess('Club Deleted');
    }


}