<?php
/**
 * ETM SOFTWARE PLC
 * Created For UNHCR with LARAVEL FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 3/3/17
 * Time: 12:56 AM
 */

namespace App\Http\Controllers\Backend\Department;

use App\Http\Controllers\Controller;
use App\Models\Department\Department;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Repositories\Backend\Department\DepartmentRepository;
use App\Http\Requests\Backend\Department\StoreDepartmentRequest;
use App\Http\Requests\Backend\Department\ManageDepartmentRequest;
use App\Http\Requests\Backend\Department\UpdateDepartmentRequest;

/**
 * Class DepartmentController
 * @package App\Http\Controllers\Backend\Budget
 */
class DepartmentController extends Controller
{

    /**
     * DepartmentController constructor.
     */
    public function __construct()
    {

    }


    /**
     * @param ManageDepartmentRequest $request
     * @param UserRepository $userRepository
     * @return mixed
     */
    public function index(ManageDepartmentRequest $request, UserRepository $userRepository)
    {

        return view('backend.department.index')
            ->withUsers($userRepository->getAll()->mapWithKeys(function ($item) {
                return [$item['id'] => $item['name']];
            }));
    }


    /**
     * @param ManageDepartmentRequest $request
     */
    public function create(ManageDepartmentRequest $request)
    {

    }


    /**
     * @param StoreDepartmentRequest $request
     * @param DepartmentRepository $departmentRepository
     * @return mixed
     */
    public function store(StoreDepartmentRequest $request, DepartmentRepository $departmentRepository)
    {
        $departmentRepository->create($request->all());
        return redirect()->route('admin.department.department.index')
                         ->withFlashSuccess('New Department Created successfully!');

    }


    /**
     * @param Department $department
     * @param ManageDepartmentRequest $request
     */
    public function show(Department $department, ManageDepartmentRequest $request)
    {

    }


    /**
     * @param Department $department
     * @param ManageDepartmentRequest $request
     * @param UserRepository $userRepository
     * @return mixed
     */
    public function edit(Department $department, ManageDepartmentRequest $request, UserRepository $userRepository)
    {
        return view('backend.department.edit')->withDepartment($department)
                                                    ->withUsers($userRepository->getAll()->mapWithKeys(function ($item) {
                                                        return [$item['id'] => $item['name']];
                                                    }));
    }


    /**
     * @param Department $department
     * @param UpdateDepartmentRequest $request
     * @param DepartmentRepository $departmentRepository
     * @return mixed
     */
    public function update(Department $department, UpdateDepartmentRequest $request, DepartmentRepository $departmentRepository)
    {
        $departmentRepository->update($department,$request->all());
        return redirect()->route('admin.department.department.index')
            ->withFlashSuccess('Department Updated successfully!');
    }


    /**
     * @param Department $department
     * @param DepartmentRepository $departmentRepository
     * @param ManageDepartmentRequest $request
     * @return mixed
     */
    public function destroy(Department $department, DepartmentRepository $departmentRepository, ManageDepartmentRequest $request)
    {
        $departmentRepository->destroy($department);
        return redirect()->route('admin.department.department.index')->withFlashSuccess('Department Deleted');
    }


}