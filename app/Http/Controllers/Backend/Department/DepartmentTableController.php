<?php

namespace App\Http\Controllers\Backend\Department;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Department\ManageDepartmentRequest;
use App\Repositories\Backend\Department\DepartmentRepository;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Access\User\UserRepository;

/**
 * Class UserTableController.
 */
class DepartmentTableController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $departments;

    /**
     * @param DepartmentRepository $departments
     * @internal param UserRepository $users
     */
    public function __construct(DepartmentRepository $departments)
    {
        $this->departments = $departments;
    }

    /**
     * @param ManageDepartmentRequest $request
     * @return mixed
     */
    public function __invoke(ManageDepartmentRequest $request)
    {
        return Datatables::of($this->departments->getForDataTable($request->get('trashed')))
            ->escapeColumns(['name', 'email'])
            ->editColumn('leader_id', function ($department) {
                return $department->user->name;
            })
            ->addColumn('actions', function ($department) {
                return $department->action_buttons;
            })
            ->withTrashed()
            ->make(true);
    }

}
