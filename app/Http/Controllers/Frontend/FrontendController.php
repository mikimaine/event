<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Auth\Auth;
use App\Http\Controllers\Controller;
use App\Models\Event\Event;
use App\Repositories\Backend\Club\ClubMembersRepository;
use App\Repositories\Backend\Club\ClubRepository;
use App\Repositories\Backend\Event\EventRepository;

/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
    /**
     * @param EventRepository $eventRepository
     * @return \Illuminate\View\View
     */
    public function index(EventRepository $eventRepository)
    {
        return view('frontend.index')->withEvents($eventRepository->getPaginated(8));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function about()
    {
        return view('frontend.about');
    }

    /**
     * @param EventRepository $eventRepository
     * @return \Illuminate\View\View
     */
    public function events(EventRepository $eventRepository)
    {
        return view('frontend.events.index')->withEvents($eventRepository->getAll());
    }

    public function eventDetail($id,EventRepository $event)
    {
        $event = $event->findOrThrowException($id);
//        dd($event->comments);
        return view('frontend.events.show')->withEvent($event)
                                                ->withComments($event->comments);
    }

    /**
     * @param ClubRepository $clubRepository
     * @return \Illuminate\View\View
     */
    public function clubs(ClubRepository $clubRepository)
    {
        return view('frontend.clubs.index')->withClubs($clubRepository->getAll());
    }

    /**
     * @param $club_id
     * @param ClubMembersRepository|ClubRepository $clubRepository
     * @return \Illuminate\View\View
     */
    public function join_clubs($club_id, ClubMembersRepository $clubRepository)
    {
        $clubRepository->create(['club_id'=>$club_id,'user_id'=>access()->id()]);
        return back()->withFlashSuccess('You joined Club successfully!');
    }

    /**
     * @param $club_id
     * @param ClubMembersRepository|ClubRepository $clubMembersRepository
     * @return \Illuminate\View\View
     */
    public function cancel_club($club_id, ClubMembersRepository $clubMembersRepository)
    {
        $clubMembersRepository->destroy($club_id);
        return back()->withFlashSuccess('You cancel Club successfully!');
    }
}
