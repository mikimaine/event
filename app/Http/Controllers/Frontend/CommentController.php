<?php

namespace App\Http\Controllers\Frontend;

use Actuallymab\LaravelComment\Models\Comment;
use App\Models\Access\User\User;
use App\Models\Event\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = Event::find($request->event);
           $comment = new Comment([
                'comment'        => $request->comment,
                'rate'           => null,
                'approved'       => true,
                'commented_id'   => access()->id(),
                'commented_type' => get_class(new User())
            ]);

        $event->comments()->save($comment);

        return back()->withSucess('Comment successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Comment|int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $comment = \App\Models\Comment\Comment::find($id)->delete();

       return back()->withFlashInfo('Comment deleted');
    }
}
