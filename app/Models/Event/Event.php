<?php

namespace App\Models\Event;

use Actuallymab\LaravelComment\Commentable;
use App\Models\Event\Traits\Relationship\EventRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Budget
 * package App.
 */
class Event extends Model
{
    use EventRelationship,
        Commentable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'event';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'department_id','type','image_path','requested_start_time','requested_end_time','budget','hall_id', 'description'];

    /**
     * @var bool
     */
    protected $canBeRated = true;

    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {

        return '<a href="'.route('admin.event.event.show', $this).'" class="btn btn-xs btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.view').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (($this->created_by == access()->id() && access()->allow('update-event')) || access()->hasRole(1)) {
        return '<a href="'.route('admin.event.event.edit', $this).'" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.edit').'"></i></a> ';
        }
    }


    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (($this->created_by == access()->id() && access()->allow('destroy-event')) || access()->hasRole(1)) {
            return '<a href="' . route('admin.event.event.destroy', $this) . '"
                 data-method="delete"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.crud.delete') . '"
                 data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
                 class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';
        }
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return
            $this->getEditButtonAttribute().
            $this->getDeleteButtonAttribute();
    }
}

