<?php

namespace App\Models\Comment;

use App\Models\Club\Traits\Relationship\ClubRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Budget
 * package App.
 */
class Comment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';

}
