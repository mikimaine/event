<?php

/**
 * ETM SOFTWARE PLC
 * Created For UNHCR with LARAVEL FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 4/16/17
 * Time: 11:29 AM
 */
namespace App\Models\Budget\Traits\Relationship;

use App\Models\Access\User\User;
use App\Models\Department\Department;

trait BudgetRelationship
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'approved_by');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }
}