<?php

namespace App\Models\CoHost;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Hall
 * package App.
 */
class CoHost extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'event_co_host';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['event_id', 'co_host_user_id'];

}
