<?php

namespace App\Models\Club;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Budget
 * package App.
 */
class ClubMembers extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'club_members';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['club_id','user_id'];
}
