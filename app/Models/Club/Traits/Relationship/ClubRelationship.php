<?php

/**
 * ETM SOFTWARE PLC
 * Created For UNHCR with LARAVEL FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 4/16/17
 * Time: 11:29 AM
 */
namespace App\Models\Club\Traits\Relationship;

use App\Models\Access\User\User;
use App\Models\Club\ClubMembers;

trait ClubRelationship
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'club_head');
    }

    public function members()
    {
        return $this->hasMany(ClubMembers::class,'club_id','id');
    }

}