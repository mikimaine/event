<?php

/**
 * ETM SOFTWARE PLC
 * Created For UNHCR with LARAVEL FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 4/14/17
 * Time: 10:06 PM
 */
namespace App\Repositories\Backend\Department;

use App\Models\Department\Department;
use App\Repositories\NewBaseRepository;
use Illuminate\Support\Facades\Auth;

class DepartmentRepository extends NewBaseRepository
{

    /**
     * Model class Name Space of the corresponding to this repository
     * @var string
     */
    protected $modelName = 'App\Models\Department\Department';

    /**
     * Associated Repository Model.
     */
    const MODEL = Department::class;

    public function getForDataTable($trashed = false)
    {

        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            ->select([
                'department.id',
                'department.name',
                'department.leader_id',
            ]);

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    public function createStub($input)
    {
        $department = new $this->modelName();

        $department->name       =  $input['name'];
        $department->leader_id  =  $input['leader_id'];
        $department->created_by =  Auth::user()->id;

        return $department;

    }
    
}