<?php

/**
 * ETM SOFTWARE PLC
 * Created For UNHCR with LARAVEL FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 4/14/17
 * Time: 10:06 PM
 */
namespace App\Repositories\Backend\Budget;

use App\Helpers\Auth\Auth;
use App\Models\Budget\Budget;
use App\Repositories\NewBaseRepository;

class BudgetRepository extends NewBaseRepository
{

    /**
     * Model class Name Space of the corresponding to this repository
     * @var string
     */
    protected $modelName = 'App\Models\Budget\Budget';

    /**
     * Associated Repository Model.
     */
    const MODEL = Budget::class;

    /**
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($trashed = false)
    {

        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            ->select([
                'buget.id',
                'buget.name',
                'buget.department_id',
                'buget.start_date',
                'buget.end_date',
                'buget.amount',
                'buget.approved_by',
            ]);

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /*
       * Creates Stub for the repository
       */
    public function createStub($input)
    {
        $budget = new $this->modelName();

        $budget->name           =  $input['name'];
        $budget->department_id  =  $input['department_id'];
        $budget->start_date     =  $input['start_date'];
        $budget->end_date       =  $input['end_date'];
        $budget->amount         =  $input['amount'];
        $budget->approved_by    =  $input['approved_by'];
        $budget->created_by     =  access()->id();

        return $budget;

    }

}