<?php

namespace App\Repositories\Backend\Event;

use App\Models\Event\Event;
use App\Repositories\NewBaseRepository;
use Illuminate\Support\Facades\Auth;

class EventRepository extends NewBaseRepository
{

    /**
     * Model class Name Space of the corresponding to this repository
     * @var string
     */
    protected $modelName = 'App\Models\Event\Event';

    /**
     * Associated Repository Model.
     */
    const MODEL = Event::class;


    /**
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($trashed = false)
    {

        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            ->select([
                'event.id',
                'event.name',
                'event.department_id',
                'event.type',
                'event.requested_start_time',
                'event.requested_end_time',
                'event.budget',
                'event.hall_id',
                'event.created_by',
                'event.description'
            ]);

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    public function checkEventDate($input)
    {
        $event = new $this->modelName();
        return $event->where('hall_id',$input['hall_id'])
                     ->where('requested_start_time',$input['requested_start_time'])
                     ->orWhere([['requested_end_time',$input['requested_end_time']],['hall_id',$input['hall_id']]])->get();
    }

    /*
     * Creates Stub for the repository
     */
    public function createStub($input)
    {
        $event = new $this->modelName();

        $event->name                    =  $input['name'];
        $event->department_id           =  $input['department_id'];
        $event->type                    =  $input['type'];
        $event->image_path              =  $input['image_path'];
        $event->requested_start_time    =  $input['requested_start_time'];
        $event->requested_end_time      =  $input['requested_end_time'];
        $event->budget                  =  $input['budget'];
        $event->hall_id                 =  $input['hall_id'];
        $event->description             =  $input['description'];
        $event->created_by              =  Auth::user()->id;

        return $event;

    }



}