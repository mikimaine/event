<?php

namespace App\Repositories\Backend\Hall;

use App\Models\Hall\Hall;
use App\Repositories\NewBaseRepository;

/**
 * Class HallRepository
 * @package App\Repositories\Backend\Hall
 */
class HallRepository extends NewBaseRepository
{

    /**
     * Model class Name Space of the corresponding to this repository
     * @var string
     */
    protected $modelName = 'App\Models\Hall\Hall';

    /**
     * Associated Repository Model.
     */
    const MODEL = Hall::class;


    /**
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($trashed = false)
    {

        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            ->select([
                'hall.id',
                'hall.name',
                'hall.max_position',
            ]);

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /*
     * Creates Stub for the repository
     */
    /**
     * @param $input
     * @return mixed
     */
    public function createStub($input)
    {
        $hall = new $this->modelName();

        $hall->name     =  $input['name'];
        $hall->max_position  =  $input['max_position'];

        return $hall;

    }



}