<?php

namespace App\Repositories\Backend\Club;

use App\Models\Club\Club;
use App\Models\Club\ClubMembers;
use App\Repositories\NewBaseRepository;

class ClubMembersRepository extends NewBaseRepository
{

    /**
     * Model class Name Space of the corresponding to this repository
     * @var string
     */
    protected $modelName = 'App\Models\Club\ClubMembers';

    /**
     * Associated Repository Model.
     */
    const MODEL = ClubMembers::class;


    /*
     * Creates Stub for the repository
     */
    public function createStub($input)
    {
        $club = new $this->modelName();

        $club->club_id     =  $input['club_id'];
        $club->user_id     =  $input['user_id'];

        return $club;

    }



}