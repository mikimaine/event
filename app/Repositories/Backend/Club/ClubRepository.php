<?php

namespace App\Repositories\Backend\Club;

use App\Models\Club\Club;
use App\Repositories\NewBaseRepository;
use Illuminate\Support\Facades\Auth;

class ClubRepository extends NewBaseRepository
{

    /**
     * Model class Name Space of the corresponding to this repository
     * @var string
     */
    protected $modelName = 'App\Models\Club\Club';

    /**
     * Associated Repository Model.
     */
    const MODEL = Club::class;


    public function getForDataTable($trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            ->select([
                'clubs.id',
                'clubs.club_name',
                'clubs.club_head',
            ]);

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery;
    }

    /*
     * Creates Stub for the repository
     */
    public function createStub($input)
    {
        $club = new $this->modelName();

        $club->club_name     =  $input['club_name'];
        $club->club_head     =  $input['club_head'];
        $club->created_by    =  Auth::user()->id;

        return $club;

    }



}