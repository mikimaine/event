<?php
/**
 * ETM SOFTWARE PLC
 * Created For UNHCR with LARAVEL FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 4/15/17
 * Time: 12:19 PM
 */

/**
 * Department Module
 */
Route::group([
    'prefix'     => 'departments',
    'as'         => 'department.',
    'namespace'  => 'Department',
], function () {

    /*
     * For DataTables
     */
    Route::post('department/get', 'DepartmentTableController')->name('department.get');
    /*
     * Department CRUD
     */
    Route::resource('department', 'DepartmentController');

});

/**
 * Budget Module
 */
Route::group([
    'prefix'     => 'budgets',
    'as'         => 'budget.',
    'namespace'  => 'Budget',
], function () {

    /*
     * For DataTables
     */
    Route::post('budget/get', 'BudgetTableController')->name('budget.get');
    /*
     * Budget CRUD
     */
    Route::resource('budget', 'BudgetController');

});

/**
 * Hall Module
 */
Route::group([
    'prefix'     => 'halls',
    'as'         => 'hall.',
    'namespace'  => 'Hall',
], function () {

    /*
     * For DataTables
     */
    Route::post('hall/get', 'HallTableController')->name('hall.get');
    /*
     * Hall CRUD
     */
    Route::resource('hall', 'HallController');

});

/**
 * Club Module
 */
Route::group([
    'prefix'     => 'clubs',
    'as'         => 'club.',
    'namespace'  => 'Club',
], function () {

    /*
     * For DataTables
     */
    Route::post('club/get', 'ClubTableController')->name('club.get');
    Route::get('club/get', 'ClubTableController')->name('club.get');
    /*
     * Club CRUD
     */
    Route::resource('club', 'ClubController');

});

/**
 * Event Module
 */
Route::group([
    'prefix'     => 'events',
    'as'         => 'event.',
    'namespace'  => 'Event',
], function () {

    /*
     * For DataTables
     */
    Route::post('event/get', 'EventTableController')->name('event.get');

    Route::get('event/co-host/{event_id}/{requested_id}','EventController@sendCoHost')->name('event.host');
    Route::get('event/co-host/{event_id}/{requested_id}','EventController@sendCoHost')->name('event.host');
    Route::get('event/accept-co-host/{event_id}/{notification_id}','EventController@acceptCoHost')->name('event.accept');
    Route::get('event/decline-co-host/{event_id}/{notification_id}','EventController@declineCoHost')->name('event.decline');
    /*
     * Event CRUD
     */
    Route::resource('event', 'EventController');

});
