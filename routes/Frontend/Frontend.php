<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'FrontendController@index')->name('index');
Route::get('/events', 'FrontendController@events')->name('events');
Route::get('/events/{id}', 'FrontendController@eventDetail')->name('event.detail');
Route::get('/clubs', 'FrontendController@clubs')->name('clubs');
Route::get('/join_club/{club_id}', 'FrontendController@join_clubs')->name('join_clubs')->middleware('auth');;
Route::get('/cancel_club/{club_id}', 'FrontendController@cancel_club')->name('cancel_club')->middleware('auth');;
Route::get('/about', 'FrontendController@about')->name('about');
Route::resource('comment', 'CommentController');

//Route::get('macros', 'FrontendController@macros')->name('macros');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
    });
});
