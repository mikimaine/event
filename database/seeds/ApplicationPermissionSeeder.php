<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

/**
 * Class EventPermissionSeeder
 */
class ApplicationPermissionSeeder extends Seeder
{

    /**
     * Because there are four general permissions for user management
     * we will start this after that !!
     */
    const SORTING_START_AFTER_NUMBER = 4;

    /**
     * @var array
     */
    private $map = array(
                            /**
                             *  CRUD Event
                             */
                            'manage-event',
                            'store-event',
                            'update-event',
                            'destroy-event',
                            'delete-event',
                            'restore-event',
                            
                            /**
                            * CRUD Department
                            */
                            'manage-department',
                            'create-department',
                            'store-department',
                            'update-department',
                            'destroy-department',
                            'delete-department',
                            'restore-department',

                            /**
                             * CRUD Budget
                             */
                            'manage-budget',
                            'create-budget',
                            'store-budget',
                            'update-budget',
                            'destroy-budget',
                            'delete-budget',
                            'restore-budget',

                            /**
                             * CRUD Hall
                             */
                            'manage-hall',
                            'create-hall',
                            'store-hall',
                            'update-hall',
                            'destroy-hall',
                            'delete-hall',
                            'restore-hall',

                            /**
                             * CRUD Clubs
                             */
                            'manage-clubs',
                            'create-clubs',
                            'store-clubs',
                            'update-clubs',
                            'destroy-clubs',
                            'delete-clubs',
                            'restore-clubs',

                            /**
                             * CRUD Comment
                             */
                            'manage-comment',
                            'create-comment',
                            'store-comment',
                            'update-comment',
                            'destroy-comment',
                            'delete-comment',
                            'restore-comment',
                        );


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Don't need to assign any permissions to administrator because the all flag is set to true
         * in RoleTableSeeder.php
         */

        /**
         * All Payroll Access Permissions
         */
        $permission_model = config('access.permission');

        /**
         * Just do the Seed!
         */
        foreach ($this->map as $key => $value) {
            $permission = new $permission_model;
            $permission->name = $value;
            $permission->display_name = $this->displayName($value);
            $permission->sort = $this->sortingKey($key);
            $permission->created_at = Carbon::now();
            $permission->updated_at = Carbon::now();
            $permission->save();
        }


    }

    /**
     * @param $key
     * @return mixed
     */
    private function sortingKey($key)
    {
        return $key + static::SORTING_START_AFTER_NUMBER;
    }

    /**
     * @param $value
     * @return string
     */
    private function displayName($value)
    {
        return ucwords(str_replace('-', ' ', $value));
    }
}
