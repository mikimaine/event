<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create Club </h4>
            </div>
            {{ Form::open(['route' => 'admin.club.club.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

                @include('backend.club.includes.partials._form',[$pageName = 'create',$buttonText = trans('buttons.general.crud.create') ])

            {{ Form::close() }}
        </div>
    </div>
</div>