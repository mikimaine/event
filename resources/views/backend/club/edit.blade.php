@extends ('backend.layouts.app')

@section ('title', 'Edit Department')

@section('after-styles')

@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Clubs</h3>

            <div class="box-tools pull-right">
                @include('backend.club.includes.partials.clubs-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">

                {{ Form::model($club,['route' => ['admin.club.club.update',$club->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}


                @include('backend.club.includes.partials._form',[$pageName = 'Update',$buttonText = trans('buttons.general.crud.update') ])


                {{ Form::close() }}
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection