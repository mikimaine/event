<div class="pull-right mb-10 hidden-sm hidden-xs">
{{--    {{ link_to_route('admin.department.department.index', 'All Departments', [], ['class' => 'btn btn-primary btn-xs']) }}--}}
    <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal">
        Create Budget
    </button>

</div><!--pull right-->

<div class="pull-right mb-10 hidden-lg hidden-md">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li>
                <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal">
                    Create Budget
                </button>
            </li>
        </ul>
    </div><!--btn group-->
</div><!--pull right-->

<div class="clearfix"></div>


