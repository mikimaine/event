<div class="modal-body">


<div class="form-group ">

    {{ Form::label('name', ' Name', ['class' => 'col-lg-2 control-label']) }}
    <div class="col-xs-4 ">
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Budget Title']) }}
    </div>
    <!--col-xs-4-->

    {{ Form::label('department_id', 'Department id', ['class' => 'col-lg-2 control-label']) }}
    <div class="col-xs-4 ">
        {{ Form::select('department_id',$departments->toArray(), null, ['class' => 'form-control','placeholder'=>'Select Department' ]) }}
{{--        {{ Form::text('grade', null, ['class' => 'form-control', 'placeholder' => 'Department Id']) }}--}}
    </div><!--col-xs-4-->
</div><!--form control-->



    <div class="form-group">
        {{ Form::label('start_date', 'Date range:', ['class' => 'col-lg-2 control-label']) }}
        {{--<label>Date range:</label>--}}
        <div class="col-xs-10">
            <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
                {{ Form::text('budget_date', null, ['class' => 'form-control pull-right','id'=>'reservation']) }}
        </div>
        <!-- /.input group -->
            </div>
    </div>

    <div class="form-group ">
        {{ Form::label('amount', 'Amount', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-xs-4 ">
            {{--{{ Form::select('leader_id',$users->toArray(), null, ['class' => 'form-control','placeholder'=>'Select Head' ]) }}--}}
            {{ Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'Amout']) }}
        </div><!--col-xs-4-->

        {{ Form::label('approved_by', 'Approved By', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-xs-4 ">
            {{ Form::select('approved_by',$users->toArray(), null, ['class' => 'form-control','placeholder'=>'Select Approved By' ]) }}
            {{--{{ Form::text('grade', null, ['class' => 'form-control', 'placeholder' => 'Grade']) }}--}}
        </div><!--col-xs-4-->

    </div><!--form control-->



</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    {{ Form::submit($buttonText, ['class' => 'btn btn-success ']) }}
</div>