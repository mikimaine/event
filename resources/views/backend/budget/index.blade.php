@extends ('backend.layouts.app')

@section ('title', 'Resume Question Management')

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
    {{ Html::style("css/backend/plugin/bootstrap-timepicker.min.css") }}
    {{ Html::style("css/backend/plugin/datepicker3.css") }}
    {{ Html::style("css/backend/plugin/daterangepicker.css") }}
    {{ Html::style("css/backend/plugin/select2.min.css") }}
@endsection

@section('page-header')
    <h1>
        Budget Management
        <small>Active Budget</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Budgets</h3>

            <div class="box-tools pull-right">
                @include('backend.budget.includes.partials.budget-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th> Title</th>
                        <th> Department</th>
                        <th> Start - End date</th>
                        <th> Amount </th>
                        <th> Approved By</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->renderType('Question') !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->

  @include('backend.budget.create');

@endsection()

@section('after-scripts')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    <!-- For Form script -->
    {{ Html::script("js/backend/plugin/jquery.inputmask.js") }}
    {{ Html::script("js/backend/plugin/jquery.inputmask.date.extensions.js") }}
    {{ Html::script("js/backend/plugin/jquery.inputmask.extensions.js") }}
    {{ Html::script("js/backend/plugin/bootstrap-colorpicker.min.js") }}
    {{ Html::script("js/backend/plugin/select2.full.min.js") }}
    {{ Html::script("js/backend/plugin/moment.min.js") }}
    {{ Html::script("js/backend/plugin/daterangepicker.js") }}
    {{ Html::script("js/backend/plugin/bootstrap-timepicker.min.js") }}
    {{ Html::script("js/backend/plugin/bootstrap-datepicker.js") }}
    {{ Html::script("js/backend/plugin/icheck.min.js") }}


    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.budget.budget.get") }}',
                    type: 'post',
                    data: {trashed: false}
                },
                columns: [
                    {data: 'name', name: 'buget.name'},
                    {data: 'department_id', name: 'buget.department_id'},
                    {data: 'budget_date', name: 'buget.start_date'},
                    {data: 'amount', name: 'buget.amount'},
                    {data: 'approved_by', name: 'buget.approved_by'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
        /**
         * for
         */
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();

            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                    {
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },
                        startDate: moment().subtract(29, 'days'),
                        endDate: moment()
                    },
                    function (start, end) {
                        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    }
            );

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false
            });
        });
    </script>
@endsection