@extends ('backend.layouts.app')

@section ('title', 'Edit Department')

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
    {{ Html::style("css/backend/plugin/bootstrap-timepicker.min.css") }}
    {{ Html::style("css/backend/plugin/datepicker3.css") }}
    {{ Html::style("css/backend/plugin/daterangepicker.css") }}
    {{ Html::style("css/backend/plugin/select2.min.css") }}
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Departments</h3>

            <div class="box-tools pull-right">

                @include('backend.event.includes.partials.event-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">

                {{ Form::model($event,['route' => ['admin.event.event.update',$event->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

                @include('backend.event.includes.partials._form',[$pageName = 'update',$buttonText = trans('buttons.general.crud.update') ])

                {{ Form::close() }}
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection


@section('after-scripts')

    <!-- For Form script -->
    {{ Html::script("js/backend/plugin/jquery.inputmask.js") }}
    {{ Html::script("js/backend/plugin/jquery.inputmask.date.extensions.js") }}
    {{ Html::script("js/backend/plugin/jquery.inputmask.extensions.js") }}
    {{ Html::script("js/backend/plugin/bootstrap-colorpicker.min.js") }}
    {{ Html::script("js/backend/plugin/select2.full.min.js") }}
    {{ Html::script("js/backend/plugin/moment.min.js") }}
    {{ Html::script("js/backend/plugin/daterangepicker.js") }}
    {{ Html::script("js/backend/plugin/bootstrap-timepicker.min.js") }}
    {{ Html::script("js/backend/plugin/bootstrap-datepicker.js") }}
    {{ Html::script("js/backend/plugin/icheck.min.js") }}


    <script>

        /**
         * for
         */
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();

            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            );

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false
            });
        });
    </script>
@endsection