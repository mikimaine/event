<div class="modal-body">
    <div class="form-group ">
        {{ Form::label('name', ' Name', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-xs-4 ">
            {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) }}
        </div>
        {{ Form::label('department_id', 'Department id', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-xs-4 ">
            {{ Form::select('department_id',$departments->toArray(), null, ['class' => 'form-control','placeholder'=>'Select Department' ]) }}
        </div><!--col-xs-4-->
    </div><!--form control-->

    <div class="form-group ">
        {{ Form::label('budget', 'Budget', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-xs-4 ">
            {{ Form::number('budget', null, ['class' => 'form-control', 'placeholder' => 'Budget']) }}
        </div>

        {{ Form::label('type', 'Event Type', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-xs-4 ">
            {{ Form::select('type',['seminar' => 'Seminar','Other' => 'Other'], null, ['class' => 'form-control','placeholder'=>'Select Event Type' ]) }}
        </div><!--col-xs-4-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('event_date', 'Date range:', ['class' => 'col-lg-2 control-label']) }}
        {{--<label>Date range:</label>--}}
        <div class="col-xs-10">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                {{ Form::text('event_date', null, ['class' => 'form-control pull-right','id'=>'reservation']) }}
            </div>
            <!-- /.input group -->
        </div>
    </div>

    <div class="form-group ">
        {{ Form::label('image', 'Image', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-xs-4 ">
            {{ Form::file('image', null, ['class' => 'form-control', 'placeholder' => 'Image']) }}
        </div>

        {{ Form::label('hall_id', 'Hall', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-xs-4 ">
            {{ Form::select('hall_id',$halls->toArray(), null, ['class' => 'form-control','placeholder'=>'Select Hall' ]) }}
        </div><!--col-xs-4-->
    </div><!--form control-->
    <div class="form-group ">
       {{ Form::label('co_host', 'Co-Host', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-xs-4 ">
            {{ Form::select('co_host',$cohost->toArray(), null, ['class' => 'form-control','placeholder'=>'Select Co-Host' ]) }}
        </div><!--col-xs-4-->
    </div><!--form control-->

    <div class="form-group ">
        {{ Form::label('description', 'Event Description', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-xs-10 ">
            {{ Form::textarea('description', null, ['class' => 'form-control','placeholder'=>'Event Description' ]) }}
        </div><!--col-xs-4-->
    </div><!--form control-->


</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    {{ Form::submit($buttonText, ['class' => 'btn btn-success ']) }}
</div>
