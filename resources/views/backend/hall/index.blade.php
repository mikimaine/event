@extends ('backend.layouts.app')

@section ('title', 'Resume Question Management')

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@endsection

@section('page-header')
    <h1>
        Hall Management
        <small>Active Hall</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Halls</h3>

            <div class="box-tools pull-right">
                @include('backend.hall.includes.partials.hall-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th> Name</th>
                        <th> Max Position</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->renderType('Question') !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->

  @include('backend.hall.create');

@endsection()

@section('after-scripts')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}

    <!-- For Form script -->
    {{ Html::script("js/backend/plugin/jquery.inputmask.js") }}
    {{ Html::script("js/backend/plugin/jquery.inputmask.date.extensions.js") }}
    {{ Html::script("js/backend/plugin/jquery.inputmask.extensions.js") }}
    {{ Html::script("js/backend/plugin/bootstrap-colorpicker.min.js") }}
    {{ Html::script("js/backend/plugin/select2.full.min.js") }}
    {{ Html::script("js/backend/plugin/moment.min.js") }}
    {{ Html::script("js/backend/plugin/daterangepicker.js") }}
    {{ Html::script("js/backend/plugin/bootstrap-timepicker.min.js") }}
    {{ Html::script("js/backend/plugin/bootstrap-datepicker.js") }}
    {{ Html::script("js/backend/plugin/icheck.min.js") }}

    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.hall.hall.get") }}',
                    type: 'post',
                    data: {trashed: false}
                },
                columns: [
                    {data: 'name', name: 'hall.name'},
                    {data: 'max_position', name: 'hall.max_position'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@endsection