@extends ('backend.layouts.app')

@section ('title', 'Edit Hall')

@section('after-styles')

@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Hall</h3>

            <div class="box-tools pull-right">
                @include('backend.hall.includes.partials.hall-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                {{ Form::model($hall,['route' => ['admin.hall.hall.update',$hall->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
                @include('backend.hall.includes.partials._form',[$pageName = 'Update',$buttonText = trans('buttons.general.crud.update') ])
                {{ Form::close() }}
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection