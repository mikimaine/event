<div class="modal-body">
<div class="form-group ">
    {{ Form::label('name', ' Name', ['class' => 'col-lg-2 control-label']) }}
    <div class="col-xs-4 ">
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) }}
    </div>
    <!--col-xs-4-->
    {{ Form::label('max_position', 'Position', ['class' => 'col-lg-2 control-label']) }}
    <div class="col-xs-4 ">
        {{--{{ Form::select('leader_id',$users->toArray(), null, ['class' => 'form-control','placeholder'=>'Select Head' ]) }}--}}
        {{ Form::number('max_position', null, ['class' => 'form-control', 'placeholder' => 'Postion']) }}
    </div><!--col-xs-4-->
</div><!--form control-->
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    {{ Form::submit($buttonText, ['class' => 'btn btn-success ']) }}
</div>