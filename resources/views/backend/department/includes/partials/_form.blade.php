<div class="modal-body">


<div class="form-group ">

    {{ Form::label('name', 'Department Name', ['class' => 'col-lg-2 control-label']) }}
    <div class="col-xs-4 ">
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Department Name']) }}
    </div>
    <!--col-xs-4-->

    {{ Form::label('leader_id', 'Department Head', ['class' => 'col-lg-2 control-label']) }}
    <div class="col-xs-4 ">
        {{ Form::select('leader_id',$users->toArray(), null, ['class' => 'form-control','placeholder'=>'Select Head' ]) }}
        {{--{{ Form::text('grade', null, ['class' => 'form-control', 'placeholder' => 'Grade']) }}--}}
    </div><!--col-xs-4-->


</div><!--form control-->




</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    {{ Form::submit($buttonText, ['class' => 'btn btn-success ']) }}
</div>