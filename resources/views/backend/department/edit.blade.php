@extends ('backend.layouts.app')

@section ('title', 'Edit Department')

@section('after-styles')

@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Departments</h3>

            <div class="box-tools pull-right">
                @include('backend.department.includes.partials.department-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">

                {{ Form::model($department,['route' => ['admin.department.department.update',$department->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}


                @include('backend.department.includes.partials._form',[$pageName = 'update',$buttonText = 'Update' ])


                {{ Form::close() }}
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection