@extends('frontend.layouts.app')

@section('content')
<div class="bg-">
    <!-- Content -->
    <div id="content"><div class="ic"></div>
        <div class="container">
            <div class="row">
                <article class="span12">
                    <h4>All Clubs</h4>
                </article>
                <div class="clear"></div>
                <table  class="table table-striped">
                    <thead>
                    <tr>
                    <th>Club name</th>
                    <th>Club leader</th>
                    <th>No of members</th>
                    <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clubs as $club)
                        <tr>
                            <td>{{ $club->club_name }}</td>
                            <td>{{ $club->user->name }}</td>
                            <td>{{ $club->members->count() }} Total members</td>
                            <?php $a = 0 ?>
                            <?php $club_memeber_id = null ?>
                            @foreach($club->members as $member)
                                @if($member->user_id == access()->id())
                                    <?php $a =1;
                                    $club_memeber_id = $member->id;
                                    ?>
                                @endif
                             @endforeach
                            @if($a == 1)
                                <td><a href="/cancel_club/{{ $club_memeber_id }}"> Cancel Club</a> </td>
                                @else
                                <td><a href="/join_club/{{ $club->id }}"> Join Club</a> </td>
                            @endif


                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
@endsection