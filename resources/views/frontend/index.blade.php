@extends('frontend.layouts.app')

@section('content')
    <div class="bg-content">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <!-- slider -->
                    <div class="flexslider">
                        <ul class="slides">
                            <li> <img src="img/slide-1.jpg" alt="" > </li>
                            <li> <img src="img/slide-2.jpg" alt="" > </li>
                            <li> <img src="img/slide-3.jpg" alt="" > </li>
                            <li> <img src="img/slide-4.jpg" alt="" > </li>
                            <li> <img src="img/slide-5.jpg" alt="" > </li>
                        </ul>
                    </div>
                    <span id="responsiveFlag"></span>
                    <div class="block-slogan">
                        <h2>Event Management System</h2>
                        <div>
                            <p>
                                Description about your Application goes here.
                            </p>
                            <p style="text-align:center; padding-top:20px;"><a href="#" class="btn btn-1"> Enjoy! </a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- content -->

        <div id="content" class="content-extra"><div class="ic"></div>
            <div class="row-1">
                <div class="container">
                    <div class="row">
                        <article class="span12">
                            <h4>Recent Events</h4>
                        </article>
                        <div class="clear"></div>
                        <ul class="portfolio clearfix">
                            @foreach($events as $event)
                                <li class="box">
                                    <a href="{{ asset('storage/'.$event->image_path) }}" class="magnifier" ><img alt=""  style="width: 270px;height: 192px;" src="{{ asset('storage/'.$event->image_path) }}"></a>
                                    <h3> <a href="/events/{{ $event->id }}">  {{ $event->name  }}  </a></h3>
                                    <h5> at {{ $event->hall->name }} Hall : {{ $event->requested_start_time }}</h5>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row-2">
                <div class="container">
                    <h3>Sample Description about WKU Event Management System</h3>
                    <h3>Easy to Use. Totally Amazing. Made with love!</h3>
                    <p>The complete template is created using the Bootstrap framework, which is highly customizable with lots of options.</p>
                    <a href="#" class="btn btn-1">Download</a>
                </div>
            </div>
            <div class="row-1">
                <div class="container">
                    <div class="row">
                        <article class="span12">
                            <h4>More Events </h4>
                        </article>
                        <ul class="thumbnails thumbnails-1">
                            <li class="span3">
                                <div class="thumbnail thumbnail-1">
                                    <h3>CS Seminar </h3>
                                    <img  src="img/blog-featured-01.jpg" alt="">
                                    <section>
                                        <a href="#"><h3>At vero eos et accusamus et iusto </h3></a>
                                        <div class="meta">
                                            <time datetime="2012-11-09" class="date-1"><i class="icon-calendar"></i> 9.11.2012</time>
                                            <div class="name-author"><i class="icon-user"></i> <a href="#">Admin</a></div>
                                            <a href="#" class="comments"><i class="icon-comment"></i> 7 comments</a>
                                        </div>
                                        <div class="clear"></div>
                                        <p>Vivamus sollicitudin libero auctor arcu pulvinar commodo.</p>
                                        <a href="#" class="btn btn-1">Read More</a> </section>
                                </div>
                            </li>
                            <li class="span3">
                                <div class="thumbnail thumbnail-1">
                                    <h3>Graduation </h3>
                                    <img  src="img/blog-featured-02.jpg" alt="">
                                    <section>
                                        <a href="#"><h3>Deleniti atque corrupti quos</h3></a>
                                        <div class="meta">
                                            <time datetime="2012-11-09" class="date-1"><i class="icon-calendar"></i> 9.11.2012</time>
                                            <div class="name-author"><i class="icon-user"></i> <a href="#">Admin</a></div>
                                            <a href="#" class="comments"><i class="icon-comment"></i> 4 comments</a>
                                        </div>
                                        <div class="clear"></div>
                                        <p>Vestibulum volutpat urna sed sapien vehicula varius.</p>
                                        <a href="#" class="btn btn-1">Read More</a> </section>
                                </div>
                            </li>
                            <li class="span3">
                                <div class="thumbnail thumbnail-1">
                                    <h3>Shega fest</h3>
                                    <img  src="img/blog-featured-03.jpg" alt="">
                                    <section>
                                        <a href="#"><h3>Similique sunt in culpa qui officia</h3></a>
                                        <div class="meta">
                                            <time datetime="2012-11-09" class="date-1"><i class="icon-calendar"></i> 9.11.2012</time>
                                            <div class="name-author"><i class="icon-user"></i> <a href="#">Admin</a></div>
                                            <a href="#" class="comments"><i class="icon-comment"></i> 9 comments</a>
                                        </div>
                                        <div class="clear"></div>
                                        <p>Pellentesque mi justo, laoreet non bibendum non, auctor imperdiet eros.</p>
                                        <a href="#" class="btn btn-1">Read More</a> </section>
                                </div>
                            </li>
                            <li class="span3">
                                <div class="thumbnail thumbnail-1">
                                    <h3 class="title-1 extra">lecture</h3>
                                    <img  src="img/blog-featured-04.jpg" alt="">
                                    <section> <a href="#"><h3>Similique sunt in culpa qui officia</h3></a>
                                        <div class="meta">
                                            <time datetime="2012-11-09" class="date-1"><i class="icon-calendar"></i> 9.11.2012</time>
                                            <div class="name-author"><i class="icon-user"></i> <a href="#">Admin</a></div>
                                            <a href="#" class="comments"><i class="icon-comment"></i> 1 comment</a>
                                        </div>
                                        <div class="clear"></div>
                                        <p>Vestibulum volutpat urna sed sapien vehicula varius.</p>
                                        <a href="#" class="btn btn-1">Read More</a> </section>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <article class="span6">
                        <h3>Shortly about us</h3>
                        <div class="wrapper">
                            <figure class="img-indent"><img src="img/15.jpg " alt="" /></figure>
                            <div class="inner-1 overflow extra">
                                <div class="txt-1">Mauris scelerisque odio quis leo viverra ac porttitor sem blandit. Sed tincidunt mattis varius. Nunc sodales ipsum nisl, eget lacinia nibh.</div>
                                Cras lacus tortor, tempus vitae porta nec, hendrerit id dolor. Nam volutpat gravida porta. Suspendisse turpis nibh, volutpat.
                            </div>
                        </div>
                    </article>
                    <article class="span6">
                        <h3>Some quick links</h3>
                        <div class="wrapper">
                            <ul class="list list-pad">
                                <li><a href="#">Campaigns</a></li>
                                <li><a href="#">Portraits</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Fine Art</a></li>
                            </ul>
                            <ul class="list list-pad">
                                <li><a href="#">Campaigns</a></li>
                                <li><a href="#">Portraits</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Fine Art</a></li>
                            </ul>
                            <ul class="list list-pad">
                                <li><a href="#">Campaigns</a></li>
                                <li><a href="#">Portraits</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Fine Art</a></li>
                            </ul>
                            <ul class="list">
                                <li><a href="#">Advertising</a></li>
                                <li><a href="#">Lifestyle</a></li>
                                <li><a href="#">Love story</a></li>
                                <li><a href="#">Landscapes</a></li>
                            </ul>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
@endsection