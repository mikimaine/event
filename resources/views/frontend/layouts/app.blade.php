<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <title> {{ app_name() }} </title>
    <meta charset="utf-8">
    <meta name="description" content="Event Management">
    <meta name="keywords" content="free, template, bootstrap, responsive">
    <meta name="author" content="Mikiyas Amdu">
        @yield('meta')

        <!-- Styles -->
        @yield('before-styles')
    {{--<link rel="stylesheet" href="{{ base_url }}theme/css/bootstrap.css" type="text/css" media="screen">--}}
    {{--<link rel="stylesheet" href="theme/css/responsive.css" type="text/css" media="screen">--}}
    {{--<link rel="stylesheet" href="theme/css/style.css" type="text/css" media="screen">--}}
    {{--<link rel="stylesheet" href="theme/css/touchTouch.css" type="text/css" media="screen">--}}
    {{--<link rel="stylesheet" href="theme/css/kwicks-slider.css" type="text/css" media="screen">--}}

        {{ Html::style('theme/css/bootstrap.css') }}
        {{ Html::style('theme/css/responsive.css') }}
        {{ Html::style('theme/css/style.css') }}
        {{ Html::style('theme/css/touchTouch.css') }}
        {{ Html::style('theme/css/kwicks-slider.css') }}
    @yield('after-styles')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    {{ Html::script('theme/js/jquery.js') }}
    {{ Html::script('theme/js/superfish.js') }}
    {{ Html::script('theme/js/jquery.flexslider-min.js') }}
    {{ Html::script('theme/js/jquery.kwicks-1.5.1.js') }}
    {{ Html::script('theme/js/jquery.easing.1.3.js') }}
    {{ Html::script('theme/js/jquery.cookie.js') }}
    {{ Html::script('theme/js/touchTouch.jquery.js') }}
    {{ Html::script('theme/js/jquery.preloader.js') }}

    {{--<script type="text/javascript">if($(window).width()>1024){document.write("<"+"script src='theme/js/jquery.preloader.js'></"+"script>");}	</script>--}}
    {{--<script type="text/javascript">if($(window).width()>1024){document.write("{{ Html::script('theme/js/jquery.preloader.js') }}");}	</script>--}}

    <script>
        jQuery(window).load(function() {
            $x = $(window).width();
            if($x > 1024)
            {
                jQuery("#content .row").preloader();    }

            jQuery('.magnifier').touchTouch();
            jQuery('.spinner').animate({'opacity':0},1000,'easeOutCubic',function (){jQuery(this).css('display','none')});
        });
        // $('body').prepend('<div id="panel"><div class="navbar navbar-inverse navbar-fixed-top" id="advanced">' +
        //                   '<span class="trigger"><strong></strong><em></em></span><div class="navbar-inner">' +
        //                   '<div class="container"><button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-top-collapse">' +
        //                   '<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>' +
        //                   '<a class="brand" href="index.html">Bootstrap</a><div class="nav-collapse collapse nav-top-collapse"><ul class="nav">' +
        //                   '<li class="home"><a href="index.html"><img src="img/tm/tm_home.png"></a></li><li class="divider-vertical"></li>' +
        //                   '<li class=""><a href="assets/scaffolding.html">Scaffolding</a></li><li class="">' +
        //                   '<a href="assets/base-css.html">Base CSS</a></li><li><a href="assets/components.html">Components</a></li>' +
        //                   '<li class=""><a href="assets/javascript.html">Javascript</a></li><li class="divider-vertical"></li>' +
        //                   '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">TM add-ons <b class="caret"></b></a>' +
        //                   '<ul class="dropdown-menu"><li><a href="404.html">Pages</a><ul><li><a href="assets/under_construction.html">Under Construction</a></li><li><a href="assets/intro.html">Intro Page</a></li><li><a href="404.html">404 page</a></li></ul></li><li><a href="assets/portfolio.html">Portfolio</a></li><li><a href="assets/slider.html">Slider</a></li><li><a href="assets/social_media.html">Social and Media<br> Sharing</a></li><li><a href="assets/css3.html">CSS3 Tricks</a></li></ul></li></ul></div></div></div></div></div>');

    </script>

    <!--[if lt IE 8]>
    <div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div>
    <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <!--<![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    {{ Html::style('theme/css/docs.css') }}
    {{ Html::style('theme/css/ie.css') }}
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
    <![endif]-->

</head>


    <body >
    <div class="spinner"></div>

            @include('includes.partials.logged-in-as')
            @include('frontend.includes.nav')


                @include('includes.partials.messages')
                @yield('content')



    <footer>
        <div class="container clearfix">
            <ul class="list-social pull-right">
                <li><a class="icon-1" href="#"></a></li>
                <li><a class="icon-2" href="#"></a></li>
                <li><a class="icon-3" href="#"></a></li>
                <li><a class="icon-4" href="#"></a></li>
            </ul>
            <div class="privacy pull-left">&copy; 2017 | <a href="#">Wolkite University Department of IT</a>
                | <a href="http://twitter.github.com/bootstrap/" target="_blank">Bootstrap</a>
                |Illustrations by <a href="http://justinmezzell.com" target="_blank"> Group 5</a></div>
        </div>
    </footer>
        <!-- Scripts -->
        @yield('before-scripts')
        {!! Html::script('theme/js/bootstrap.js') !!}
        @yield('after-scripts')

        @include('includes.partials.ga')
    </body>
</html>