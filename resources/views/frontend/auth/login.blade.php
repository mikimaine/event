@extends('frontend.layouts.app')

@section('content')
    <div class="bg-contents">
        <!-- content -->
        <div id="content"><div class="ic"></div>
            <div class="container">
    <div class="row">

        <div class="col-md-6 col-md-offset-2">

            <div class="panel panel-default">
                <h1 class="panel-heading">{{ trans('labels.frontend.auth.login_box_title') }}</h1>
                    <br>
                <div class="panel-body">


                    {{ Form::open(['route' => 'frontend.auth.login', 'class' => 'form-horizontal','id'=>'']) }}

                    <div class="form-group">
                        {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            &nbsp;&nbsp;&nbsp; {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->
                    <br>

                    <div class="form-group">
                        {{ Form::label('password', trans('validation.attributes.frontend.password'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                          &nbsp;&nbsp;&nbsp;  {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->
                    <br>
                    <div class="form-group">

                        <div class="col-md-6">
                            <div class="checkbox">
                                <label class="">
                                   {{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}
                                </label>
                            </div>
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        <div class="col-sm-6 controls">
                            {{ Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'btn btn-success']) }}
                            <br>
                        </div><!--col-md-6-->
                        <br/>
                            {{ link_to_route('frontend.auth.password.reset', trans('labels.frontend.passwords.forgot_password')) }}

                    </div><!--form-group-->

                    {{ Form::close() }}

                    <div class="row text-center">
                        {!! $socialite_links !!}
                    </div>
                </div><!-- panel body -->

            </div><!-- panel -->

        </div><!-- col-md-8 -->

    </div><!-- row -->
</div>
    </div>
    </div>

@endsection