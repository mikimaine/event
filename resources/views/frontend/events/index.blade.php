@extends('frontend.layouts.app')

@section('content')
<div class="bg-content">
    <!-- Content -->
    <div id="content"><div class="ic"></div>
        <div class="container">
            <div class="row">
                <article class="span12">
                    <h4>Latest Events</h4>
                </article>
                <div class="clear"></div>
                <ul class="portfolio clearfix">
                    @foreach($events as $event)
                    <li class="box">
                         <a href="{{ asset('storage/'.$event->image_path) }}" class="magnifier" ><img alt=""  style="width: 270px;height: 192px;" src="{{ asset('storage/'.$event->image_path) }}"></a>
                            <h3> <a href="/events/{{ $event->id }}">  {{ $event->name  }}  </a></h3>
                            <h5> at {{ $event->hall->name }} Hall : {{ $event->requested_start_time }}</h5>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection