@if ($logged_in_user)
<div class="">
    <div class="row">
        <div class="col-md-6">
            <div class="widget-area no-padding blank">
                <div class="status-upload">
                    {{ Form::open(['route' => 'frontend.comment.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
                    {{ Form::hidden('event', $event->id) }}
                    <textarea name="comment" class="form-control" placeholder="What is your though right now?" ></textarea>
                        <ul>
                            <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Audio"><i class="fa fa-music"></i></a></li>
                            <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Video"><i class="fa fa-video-camera"></i></a></li>
                            <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Sound Record"><i class="fa fa-microphone"></i></a></li>
                            <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Picture"><i class="fa fa-picture-o"></i></a></li>
                        </ul>
                        <button type="submit" class="btn btn-success green"><i class="fa fa-share"></i> Comment</button>
                    {{ Form::close() }}

                </div><!-- Status Upload  -->
            </div><!-- Widget Area -->
        </div>

    </div>
</div>
@else
    <h5> Only Logged in User can comment. </h5>
@endif

<div class="">
    <div class="row">
        @foreach($comments as $comment)
        <div class="col-sm-8">
            <div class="panel panel-white post panel-shadow">
                <div class="post-heading">
                    <div class="pull-left image">
                        <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar" alt="user profile image">
                    </div>
                    <div class="pull-left meta">
                        <div class="title h5">
                            <a href="#"><b>{{ App\Models\Access\User\User::find($comment->commented_id)->name }}</b></a>
                            made a comment.
                        </div>
                        <h6 class="text-muted time">{{$comment->created_at->diffForHumans() }}</h6>
                    </div>
                </div>
                <div class="post-description">
                    <p>{{ $comment->comment }}<p>
                    @role(1)
                    <div class="stats">
                        {{ Form::open(['route'=>['frontend.comment.destroy', $comment->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'DELETE']) }}
                            <button type="submit" class="btn btn-default stat-item">
                                Delete
                            </button>
                        {{ Form::close() }}
                    </div>
                    @endauth
                </div>
            </div>
        </div>
         <br>
        @endforeach

    </div>
</div>