<header>
    <div class="container clearfix">
        <div class="row">
            <div class="span12">
                <div class="navbar navbar_">
                    <div class="container">
                        <h1 class="brand brand_"><a href="/"><img alt="" src="/theme/img/logo.png"> </a></h1>
                        {{--{{ link_to_route('frontend.index', app_name(), [], ['class' => 'navbar-brand']) }}--}}
                        <a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">Menu <span class="icon-bar"></span> </a>
                        <div class="nav-collapse nav-collapse_  collapse">
                            <ul class="nav sf-menu">
                                <li class=""><a href="/">Home</a></li>
                                <li><a href="/events">Events</a></li>
                                <li><a href="/clubs">Clubs</a></li>
                                <li><a href="/about">About</a></li>
                                {{--<li class="sub-menu"><a href="process.html">Process</a>--}}
                                    {{--<ul>--}}
                                        {{--<li><a href="#">Process 01</a></li>--}}
                                        {{--<li><a href="#">Process 02</a></li>--}}
                                        {{--<li><a href="#">Process 03</a></li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                                {{--<li><a href="#">Contact</a></li>--}}
                                @if ($logged_in_user)
                                    <li>{{ link_to_route('frontend.user.dashboard', trans('navs.frontend.dashboard')) }}</li>
                                @endif

                                @if (! $logged_in_user)
                                    <li>{{ link_to_route('frontend.auth.login', trans('navs.frontend.login')) }}</li>

                                    @if (config('access.users.registration'))
                                        <li>{{ link_to_route('frontend.auth.register', trans('navs.frontend.register')) }}</li>
                                    @endif
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            {{ $logged_in_user->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            @permission('view-backend')
                                            <li>{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                                            @endauth

                                            <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account')) }}</li>
                                            <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

